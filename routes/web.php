<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin','middleware'=>['auth'=>'admin']],function (){
    Route::get('/',function (){
       return view('admin');
    });
    Route::get('/cliente','ClienteController@panel');
    Route::get('/cliente/registrar','ClienteController@register');
    Route::post('/cliente/create','ClienteController@create');
    Route::get('/cliente/mostrar/{state}','ClienteController@show');
    Route::get('/cliente/desactivar/{id}','ClienteController@disable');
    Route::get('/cliente/activar/{id}','ClienteController@enable');
    Route::get('/cliente/modificar/{id}','ClienteController@modify');
    Route::post('/cliente/change/{id}','ClienteController@change');

    Route::get('/proveedor','ProveedorController@panel');
    Route::get('/proveedor/registrar','ProveedorController@register');
    Route::post('/proveedor/create','ProveedorController@create');
    Route::get('/proveedor/mostrar/{state}','ProveedorController@show');
    Route::get('/proveedor/desactivar/{id}','ProveedorController@disable');
    Route::get('/proveedor/activar/{id}','ProveedorController@enable');
    Route::get('/proveedor/modificar/{id}','ProveedorController@modify');
    Route::post('/proveedor/change/{id}','ProveedorController@change');

    Route::get('/sucursal','SucursalController@panel');
    Route::get('/sucursal/registrar','SucursalController@register');
    Route::post('/sucursal/create','SucursalController@create');
    Route::get('/sucursal/mostrar/{state}','SucursalController@show');
    Route::get('/sucursal/desactivar/{id}','SucursalController@disable');
    Route::get('/sucursal/activar/{id}','SucursalController@enable');
    Route::get('/sucursal/modificar/{id}','SucursalController@modify');
    Route::post('/sucursal/change/{id}','SucursalController@change');

    Route::get('/servicio','ServicioController@panel');
    Route::get('/servicio/registrar','ServicioController@register');
    Route::post('/servicio/create','ServicioController@create');
    Route::get('/servicio/mostrar/{state}','ServicioController@show');
    Route::get('/servicio/desactivar/{id}','ServicioController@disable');
    Route::get('/servicio/activar/{id}','ServicioController@enable');
    Route::get('/servicio/modificar/{id}','ServicioController@modify');
    Route::post('/servicio/change/{id}','ServicioController@change');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


