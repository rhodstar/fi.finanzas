# ServiPay

```shell
octubre 2019
HTML 5 | js | css3 | bootstrap v4.1 | laravel 6.0.3 | php7.3 | laravel/installer2.1
```
<!-- Stupid test -->
##  Contribuciones

La guía para contribuir al proyecto se encuentre [aquí](CONTRIBUTING.md)

## Uso para desarrolladores

En el archivo [INSTALL](INSTALL.md) se ofrece una guía rápida y general de como instalar las dependencias necesarias para trabajar con el proyecto.

## Licencia

Este proyecto esta licenciado aún no tiene una licencia pero se encontrará  [aquí](LICENSE)

## To do for tomorrow

* Agregar más usuarios con `role` `common`, para ello se modifica el archivo ClienteSeeeder.php. Solo hay que copiar y pegar muchas veces lo siguiente.

  ```php
  User::create(
      [
          'name' => 'Pablo Lobato',
          'telefono' => 9452323021,
          'email' => 'palo@gmail.com',
          'es_activo' => true,
          'password' => Hash::make('inbursa123'),
          'role'=>'common'
      ]
  );
  ```

* El único usuario administrador el `Rodrigo Francisco` con correo `rho@gmail.com`, la contraseña es `admin123`, dicho usuario es el único que puede acceder a
  * `127.0.0.1:8000/admin/cliente` en esta vista se mostrarán las opciones que tiene el administrador para realizarle al cliente
    * Ver Activos `127.0.0.1:8000/admin/cliente/activos`
    * Ver Desahibilitados (Pasivos), son clientes que han sido dado de baja pero no se eliminaron de la base. `127.0.0.1:8000/admin/cliente/pasivos`
    * Para registrar un cliente se hacer desde `127.0.0.1:8000/register`, por el momento ningún usuario debe estar logeado para que los clientes se registren.

