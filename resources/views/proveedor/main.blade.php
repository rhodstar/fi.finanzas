@extends('layouts.app')

@section('content')
    <div class="container py-4">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>Administración de proveedores</h3>
            </div>
        </div>

        <div class="row py-4 pb-3">
            <div class="col-sm-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ver proveedores activos</h5>
                        <p class="card-text">Esta opción permite ver a los proveedores activos de servipay.</p>
                        <a href="{{ url('/admin/proveedor/mostrar/activos') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ver proveedores pasivos</h5>
                        <p class="card-text">Esta opción permite a ver a los proveedores que fueron desactivados.</p>
                        <a href="{{ url('/admin/proveedor/mostrar/pasivos') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Registrar proveedor</h5>
                        <p class="card-text">Registra un proveedor en la base de datos de servipay</p>
                        <a href="{{ url('/admin/proveedor/registrar') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <!--a href="{{url('/admin/proveedor')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Clientes</a-->
            </div>
        </div>
    </div>
@endsection
