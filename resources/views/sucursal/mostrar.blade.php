@extends('layouts.app')

@section('content')
<div class="container py-4 pb-5">
    @isset($error)
        <div class="row mb-3">
            <h3>{{$error}}</h3>
        </div>
    @else
        <div class="row mb-3">
            <h3>Los Sucursales {{$state}}s de servipay son</h3>
        </div>
        <div class="row pb-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Direccion</th>
                    <th scope="col">Telefono</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach( $sucursales as $sucursal)
                    <tr>
                        <th scope="row">{{$loop->index + 1}}</th>
                        <td>{{$sucursal['nombre']}}</td>
                        <td>{{$sucursal['direccion']}}</td>
                        <td>{{$sucursal['telefono']}}</td>
                        <td>
                            @if($state == 'activo')
                            <a href="{{ url('/admin/sucursal/desactivar/'.$sucursal['id']) }}" class="btn btn-danger">Dar de baja</a>
                            @else
                            <a href="{{ url('/admin/sucursal/activar/'.$sucursal['id']) }}" class="btn btn-success">Activar</a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('/admin/sucursal/modificar/'.$sucursal['id']) }}" class="btn btn-outline-success">Modificar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <a href="{{url('/admin/sucursal')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Sucursal</a>
            </div>
        </div>
        <div class="row pb-5 mb-5">

        </div>
    @endisset
</div>
@endsection
