@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Registrar servicio') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ url('/admin/servicio/create') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                <div class="col-md-6">
                                    <input id="nombre" type="text" class="form-control @error('name') is-invalid @enderror" name="nombre"
                                           value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                    @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="tipo" class="col-md-4 col-form-label text-md-right">{{ __('Tipo') }}</label>

                                <div class="col-md-6">
                                    <input id="tipo" type="text" class="form-control @error('tipo') is-invalid @enderror"
                                           name="tipo" value="{{ old('tipo') }}" required autocomplete="tipo">

                                    @error('tipo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="comision_distribuidor" class="col-md-4 col-form-label text-md-right">{{ __('Comisión de distribuidor') }}</label>

                                <div class="col-md-6">
                                    <input id="comision_distribuidor" type="text" class="form-control @error('comision_distribuidor') is-invalid @enderror"
                                           name="comision_distribuidor" value="{{ old('comision_distribuidor') }}" required autocomplete="comision_distribuidor">

                                    @error('comision_distribuidor')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="comision_usuario" class="col-md-4 col-form-label text-md-right">{{ __('Comisión de usuario') }}</label>

                                <div class="col-md-6">
                                    <input id="comision_usuario" type="text" class="form-control @error('comision_usuario') is-invalid @enderror"
                                           name="comision_usuario" value="{{ old('comision_usuario') }}" required autocomplete="comision_usuario">

                                    @error('comision_usuario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Registrar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <a href="{{url('/admin/servicio')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Servicio</a>
            </div>
        </div>
    </div>
@endsection
