@extends('layouts.app')

@section('content')
    <div class="container py-4">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>Panel Administración</h3>
            </div>
        </div>

        <div class="row py-4">
            <div class="col-sm-6 pb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Administrar clientes</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="{{ url('/admin/cliente/') }}" class="btn btn-primary">Ir</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 pb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Administrar Proveedores</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="{{ url('/admin/proveedor/') }}" class="btn btn-primary">Ir</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 pb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Administrar surcusales</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="{{ url('/admin/sucursal') }}" class="btn btn-primary">Ir</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 pb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Administrar servicios</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <a href="{{ url('/admin/servicio') }}" class="btn btn-primary">Ir</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
