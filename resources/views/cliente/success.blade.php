@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="row justify-content-center pb-3">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Mensaje</div>

                    <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            {{ $message }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <a href="{{url('/admin/cliente')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Clientes</a>
            </div>
        </div>
    </div>
@endsection
