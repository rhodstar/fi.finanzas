@extends('layouts.app')

@section('content')
    <div class="container py-4">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>Administración de clientes</h3>
            </div>
        </div>

        <div class="row py-4">
            <div class="col-sm-6 pb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ver clientes activos</h5>
                        <p class="card-text">Esta opción permite ver a los clientes activos de servipay.</p>
                        <a href="{{ url('/admin/cliente/mostrar/activos') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 pb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Ver clientes pasivos</h5>
                        <p class="card-text">Esta opción permite a ver a los clientes que fueron desactivados.</p>
                        <a href="{{ url('/admin/cliente/mostrar/pasivos') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 pb-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Registrar cliente</h5>
                        <p class="card-text">Registra un cliente en la base de datos de servipay</p>
                        <a href="{{ url('/admin/cliente/registrar') }}" class="btn btn-primary">Ver</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <!--a href="{{url('/admin/cliente')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Clientes</a-->
            </div>
        </div>

    </div>
@endsection
