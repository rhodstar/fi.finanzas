@extends('layouts.app')

@section('content')
<div class="container py-4 pb-5">
    @isset($error)
        <div class="row">
            <h3>{{$error}}</h3>
        </div>
    @else
        <div class="row mb-3">
            <h3>Los clientes {{$state}}s de servipay son</h3>
        </div>
        <div class="row py-2">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Telefono</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @foreach( $clientes as $cliente)
                    <tr>
                        <th scope="row">{{$loop->index + 1}}</th>
                        <td>{{$cliente['name']}}</td>
                        <td>{{$cliente['email']}}</td>
                        <td>{{$cliente['telefono']}}</td>
                        <td>
                            @if($state == 'activo')
                            <a href="{{ url('/admin/cliente/desactivar/'.$cliente['id']) }}" class="btn btn-danger">Dar de baja</a>
                            @else
                            <a href="{{ url('/admin/cliente/activar/'.$cliente['id']) }}" class="btn btn-success">Activar</a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ url('/admin/cliente/modificar/'.$cliente['id']) }}" class="btn btn-outline-success">Modificar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12 col-12 text-center">
                <a href={{url('/admin')}} class="btn btn-primary" role="button" aria-pressed="true">Ir al Panel Principal</a>
                <a href="{{url('/admin/cliente')}}" class="btn btn-secondary" role="button" aria-pressed="true">Ir al Panel de Clientes</a>
            </div>
        </div>
        <div class="row pb-5 mb-5">

        </div>
    @endisset
</div>
@endsection
