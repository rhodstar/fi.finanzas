 <?php
use App\Servicio;
use Illuminate\Database\Seeder;
class ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Servicio::create(
            [
                'nombre' => 'Reparacion de la Red de datos',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$14.00',
                'comision_usuario' => '$20.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Cable / Internet SKY',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$4.50 ',
                'comision_usuario' => '$9.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Luz CFE',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.50',
                'comision_usuario' => '$8.50'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Telefonia MAXCOM',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.00',
                'comision_usuario' => '$8.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Telefonia / Cable / Internet TELMEX ',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.50',
                'comision_usuario' => '$12.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Telefonia / Cable / Internet TOTALPLAY',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.00',
                'comision_usuario' => '$8.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Television MEGACABLE',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$4.50',
                'comision_usuario' => '$9.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Television VETV',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$4.50',
                'comision_usuario' => '$9.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Television CABLEMS',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.00',
                'comision_usuario' => '$8.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Telefonia/Cable/ Internet AXTEL ',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$0.00',
                'comision_usuario' => '$0.00'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Gas GASNATURAL',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$5.50',
                'comision_usuario' => '$5.50'
            ]
        );
        Servicio::create(
            [
                'nombre' => 'Crédito INFONAVIT',
                'es_activo' => true,
                'tipo' => 'nose',
                'comision_distribuidor' => '$6.00',
                'comision_usuario' => '$8.00'
            ]
        );

    }
}
