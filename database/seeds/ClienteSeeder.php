<?php

use App\User;
use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Rodrigo Francisco',
                'telefono' => 9452323021,
                'email' => 'rho@gmail.com',
                'es_activo' => true,
                'password' => Hash::make('admin123'),
                'role'=>'admin'
            ]
        );

        User::create(
            [
                'name' => 'Andres Marqueda',
                'telefono' => 9452323021,
                'email' => 'andresmes@gmail.com',
                'es_activo' => true,
                'password' => Hash::make('inbursa123'),
                'role'=>'common'
            ]
        );
        User::create(
            [
                'name' => 'Mario Molina',
                'telefono' => 2134230191,
                'email' => 'mariopremios@gmail.com',
                'es_activo' => true,
                'password' => Hash::make('inbursa123'),
                'role' => 'common'

            ]
        );
	User::create(
	   [
        	'name' => 'Josue Viramontes',
        	'telefono' => 5523572529,
        	'email' => 'josue1999sider@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('inbursa234'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Yafte Ramírez',
        	'telefono' => 75946569,
        	'email' => 'josue1999s@hotmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('inbursa3'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Jacob Rodriguez',
        	'telefono' => 55235725334,
        	'email' => 'jacob@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('inbursa004'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Mauricio Hernandez',
        	'telefono' => 651525872,
        	'email' => 'mauaczino@hotmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('inbursa666'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Abigail Solis',
        	'telefono' => 5523572527,
        	'email' => 'aby@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('aby777'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Leslie Ibarra',
        	'telefono' => 65590316,
        	'email' => 'Leslie_99@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('Lesaleida'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Isaias Pimentel',
        	'telefono' => 651525872,
        	'email' => 'isaias172@hotmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('inbur00sa99'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Lorelly Ramírez',
        	'telefono' => 5523574568,
        	'email' => 'aby_lorelly@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('mafalda67'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Efran Vazquez',
        	'telefono' => 65982316,
        	'email' => 'efra_vaz@outlook.com',
        	'es_activo' => true,
        	'password' => Hash::make('efva2019'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Ruben Mendez',
        	'telefono' => 428525872,
        	'email' => 'elrabos@hotmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('1234509876'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Alfredo Miramontes',
        	'telefono' => 5523570020,
        	'email' => 'astroboy@gmail.com',
        	'es_activo' => true,
        	'password' => Hash::make('astroboy1974'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Aleida Gomez',
        	'telefono' => 98720316,
        	'email' => 'aleida2003@outlook.com',
        	'es_activo' => true,
        	'password' => Hash::make('315253427'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Aleida Hernandez',
        	'telefono' => 60023319,
        	'email' => 'alhe_19@outlook.com',
        	'es_activo' => true,
        	'password' => Hash::make('aleida.he19'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Cecilia Gomez',
        	'telefono' => 98750316,
        	'email' => 'cecyB@outlook.com',
        	'es_activo' => true,
        	'password' => Hash::make('cesgoB'),
        	'role'=>'common'
	    ]
	);
	User::create(
	   [
        	'name' => 'Monica Villeda',
        	'telefono' => 60023555,
        	'email' => 'mon@outlook.com',
        	'es_activo' => true,
        	'password' => Hash::make('1996.monvic'),
        	'role'=>'common'
	    ]
	);
    }
}
