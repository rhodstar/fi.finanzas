<?php
use App\Sucursal;
use Illuminate\Database\Seeder;
class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sucursal::create(
            [
                'nombre' => 'Universidad',
                'direccion' => 'Av. cerro del agua, Ciudad Univesidaria, CDMX',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Copilco',
                'direccion' => 'Avenida siempre viva n�m. 34 Copilco CDMX',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Iztapalapa',
                'direccion' => 'Calle de la muerte n�m. 5 Tezonco Iztapalapa',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Nezahualcoyotl',
                'direccion' => 'Calle de la vida num. 66 Nezahualcoyotl',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Chalco',
                'direccion' => 'Calle de la bruja num. 12 Chalco Edo. Mex',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Ecatepec',
                'direccion' => 'Av. Pino  Prizo 2',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'Ciudad Azteca',
                'direccion' => 'Avenida Pichardo Nezahualpilli',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'PACHUCA',
                'direccion' => 'HIDALGO No. 1, ESQ. MORELOS',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => '1� DE MAYO',
                'direccion' => 'Av. 1� de Mayo 228, Col. San Andr�s Atoto Naucalpan de Ju�rez',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'BOSQUE REAL',
                'direccion' => 'Carretera M�xico Huixquilucan No.180 Col. San Bartolom� Coatepec',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'CENTRO SAN CRIST�BAL',
                'direccion' => 'Blvd. Insurgentes S/N Local A10 Col. San Crist�bal ',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'CENTRO SAN MIGUEL',
                'direccion' => 'Circuitos M�dicos No. 9 Lote 10 Mz. 204 Zona uno B Frac. Cd. Sat�lite',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'CUAUTITL�N',
                'direccion' => 'Av. Primera de Mayo S/N Mz. C34C Col. Centro Urbano, Centro Comercial Luna Parc',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'INTERLOMAS',
                'direccion' => 'Lote 16 de la manzana III en la V�a Magna del Centro Urbano San Fernando La Herradura',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => ' JARDINES DE TULTITL�N',
                'direccion' => 'Av. Prados #100 Col. San Pablo de las Salinas, Tultitlan Edo De M�xico',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'LA FLORIDA',
                'direccion' => 'Blvd. Manuel Avila Camacho 3131 Lot. 37 Mz. 25, Frac. La Florida',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'LOMAS VERDES',
                'direccion' => 'Av. Lomas Verdes No. 414 Loc. B1, B2, B2 y 137 Col. Santiago Occipaco',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'METEPEC',
                'direccion' => 'Av. Tecnol�gico No. 1600 Nte. Local Sub Ancla 2 Col. Salvador Tizatlalli ',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => ' MULTIPLAZA ALAMEDAS',
                'direccion' => 'Blvd. Adolfo L�pez Mateos No. 100 Frac. Las Alamedas',
                'es_activo' => true
            ]
        );
        Sucursal::create(
            [
                'nombre' => 'MUNDO E',
                'direccion' => 'Autopista M�xico-Qro. No. 1007 Ex Hacienda de Santa M�nica',
                'es_activo' => true
            ]
        );

    }
}
