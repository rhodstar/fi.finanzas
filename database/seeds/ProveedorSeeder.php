<?php
use App\Proveedor;
use Illuminate\Database\Seeder;
class ProveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proveedor::create(
            [
                'nombre' => 'AGUA',
                'telefono' => 5532164904,
                'email' => 'AGUA@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre'=>'ALSEA',
                'telefono' => 5519390782,
                'email' => 'ALSEA@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'LACOMER',
                'telefono' => 5538402920,
                'email' => 'LACOMER@gmail.com',
                'es_activo' => true
            ]
        );
            Proveedor::create(
            [
                'nombre' => 'LAMOSA',
                'telefono' => 5535164904,
                'email' => 'LAMOSA@gmail.com',
                'es_activo' => false
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'ORBIA',
                'telefono' => 5519390784,
                'email' => 'ORBIA@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'OMA',
                'telefono' => 5538402928,
                'email' => 'OMA@gmail.com',
                'es_activo' => true
            ]
        );
            Proveedor::create(
            [
                'nombre' => 'TLEVISA',
                'telefono' => 5532164906,
                'email' => 'TLEVISA@gmail.com',
                'es_activo' => false
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'TRAXION',
                'telefono' => 5519390785,
                'email' => 'TRAXION@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'WALMEX	',
                'telefono' => 5538402909,
                'email' => 'WALMEX@gmail.com',
                'es_activo' => true
            ]
        );
            Proveedor::create(
            [
                'nombre' => 'SPORT',
                'telefono' => 5532164909,
                'email' => 'SPORT@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'SIMEC',
                'telefono' => 5519390762,
                'email' => 'jSIMEC@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'GSANBOR',
                'telefono' => 5538405480,
                'email' => 'GSANBOR@gmail.com',
                'es_activo' => true
            ]
        );
            Proveedor::create(
            [
                'nombre' => 'GCARSO',
                'telefono' => 553973904,
                'email' => 'GCARSO@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'GAP',
                'telefono' => 7649390782,
                'email' => 'GAP@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'MAXCOM	',
                'telefono' => 5538092920,
                'email' => 'MAXCOM@gmail.com',
                'es_activo' => true
            ]
        );
            Proveedor::create(
            [
                'nombre' => 'MEGA',
                'telefono' => 5532199904,
                'email' => 'MEGA@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'FRAGUA',
                'telefono' => 5519390702,
                'email' => 'FRAGUA@gmail.com',
                'es_activo' => true
            ]
        );
        Proveedor::create(
            [
                'nombre' => 'FIBRATC',
                'telefono' => 5538409020,
                'email' => 'FIBRATCo@gmail.com',
                'es_activo' => true
            ]
        );

	}
}
