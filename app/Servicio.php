<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $fillable = [
        'nombre', 'tipo','telefono','comision_distribuidor','comision_usuario'
    ];
    public function sucursal(){
        return $this->belongsTo('App\Sucursal');
    }
}
