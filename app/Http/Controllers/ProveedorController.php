<?php

namespace App\Http\Controllers;

use App\Proveedor;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function panel(){
        return view('proveedor.main');
    }

    public function register(){
        return view('proveedor.registrar');
    }

    public function create(Request $request){
        Proveedor::create($request->all());
        return view('proveedor.success')->with('message','El proveedor fue registrado exitosamente');
    }

    public function show($state){

        if ($state == 'activos'){
            return view('proveedor.mostrar')
                ->with('proveedores',Proveedor::where('es_activo',true)->get())
                ->with('state','activo');
        }
        if ($state == 'pasivos'){
            //dd($state);
            return view('proveedor.mostrar')
                ->with('proveedores',Proveedor::where('es_activo',false)->get())
                ->with('state','pasivo');
        }

        return view('proveedor.mostrar')
            ->with('error','La vista no ha sido encontrada');


    }

    public function disable($id){
        Proveedor::where('id', $id)
            ->update(['es_activo' => false]);
        return redirect('/admin/proveedor/mostrar/activos');
    }

    public function enable($id){
        Proveedor::where('id', $id)
            ->update(['es_activo' => true]);
        return redirect('/admin/proveedor/mostrar/pasivos');
    }

    public function modify($id){
        $proveedor = Proveedor::where('id', $id)->first();
        return view('proveedor.modificar')
            ->with('proveedor',$proveedor);
    }
    public function change($id,Request $request){
        $data = $request->all();
        Proveedor::where('id', $id)
            ->update([
                'nombre' => $data['nombre'],
                'email' => $data['email'],
                'telefono' => $data['telefono']
            ]);
        return view('proveedor.success')
            ->with('message','proveedor modificado exitosamente');
    }
}
