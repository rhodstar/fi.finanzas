<?php

namespace App\Http\Controllers;

use App\Sucursal;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function panel(){
        return view('sucursal.main');
    }

    public function register(){
        return view('sucursal.registrar');
    }

    public function create(Request $request){
        Sucursal::create($request->all());
        return view('sucursal.success')->with('message','El sucursal fue registrado exitosamente');
    }

    public function show($state){

        if ($state == 'activos'){
            return view('sucursal.mostrar')
                ->with('sucursales',Sucursal::where('es_activo',true)->get())
                ->with('state','activo');
        }
        if ($state == 'pasivos'){
            //dd($state);
            return view('sucursal.mostrar')
                ->with('sucursales',Sucursal::where('es_activo',false)->get())
                ->with('state','pasivo');
        }

        return view('sucursal.mostrar')
            ->with('error','La vista no ha sido encontrada');


    }

    public function disable($id){
        Sucursal::where('id', $id)
            ->update(['es_activo' => false]);
        return redirect('/admin/sucursal/mostrar/activos');
    }

    public function enable($id){
        Sucursal::where('id', $id)
            ->update(['es_activo' => true]);
        return redirect('/admin/sucursal/mostrar/pasivos');
    }

    public function modify($id){
        $sucursal = Sucursal::where('id', $id)->first();
        return view('sucursal.modificar')
            ->with('sucursal',$sucursal);
    }
    public function change($id,Request $request){
        $data = $request->all();
        Sucursal::where('id', $id)
            ->update([
                'nombre' => $data['nombre'],
                'direccion' => $data['direccion'],
                'telefono' => $data['telefono']
            ]);
        return view('sucursal.success')
            ->with('message','sucursal modificado exitosamente');
    }
}
