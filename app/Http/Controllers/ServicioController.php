<?php

namespace App\Http\Controllers;

use App\Servicio;
use Illuminate\Http\Request;

class ServicioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function panel(){
        return view('servicio.main');
    }

    public function register(){
        return view('servicio.registrar');
    }

    public function create(Request $request){
        Servicio::create($request->all());
        return view('servicio.success')->with('message','El servicio fue registrado exitosamente');
    }

    public function show($state){

        if ($state == 'activos'){
            return view('servicio.mostrar')
                ->with('servicios',Servicio::where('es_activo',true)->get())
                ->with('state','activo');
        }
        if ($state == 'pasivos'){
            //dd($state);
            return view('servicio.mostrar')
                ->with('servicios',Servicio::where('es_activo',false)->get())
                ->with('state','pasivo');
        }

        return view('servicio.mostrar')
            ->with('error','La vista no ha sido encontrada');


    }

    public function disable($id){
        Servicio::where('id', $id)
            ->update(['es_activo' => false]);
        return redirect('/admin/servicio/mostrar/activos');
    }

    public function enable($id){
        Servicio::where('id', $id)
            ->update(['es_activo' => true]);
        return redirect('/admin/servicio/mostrar/pasivos');
    }

    public function modify($id){
        $servicio = Servicio::where('id', $id)->first();
        return view('servicio.modificar')
            ->with('servicio',$servicio);
    }
    public function change($id,Request $request){
        $data = $request->all();
        Servicio::where('id', $id)
            ->update([
                'nombre' => $data['nombre'],
                'tipo' => $data['tipo'],
                'comision_distribuidor' => $data['comision_distribuidor'],
                'comision_usuario' => $data['comision_usuario']
            ]);
        return view('servicio.success')
            ->with('message','servicio modificado exitosamente');
    }
}
