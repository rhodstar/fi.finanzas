<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function panel(){
        return view('cliente.main');
    }

    public function register(){
        return view('cliente.registrar');
    }

    public function create(Request $request){
        User::create($request->all());
        return view('cliente.success')->with('message','El cliente fue registrado exitosamente');
    }

    public function show($state){

        if ($state == 'activos'){
            return view('cliente.mostrar')
                ->with('clientes',User::where('es_activo',true)->where('role','common')
                ->get())
                ->with('state','activo');
        }
        if ($state == 'pasivos'){
            //dd($state);
            return view('cliente.mostrar')
                ->with('clientes',User::where('es_activo',false)->where('role','common')
                ->get())
                ->with('state','pasivo');
        }

        return view('cliente.mostrar')
            ->with('error','La vista no ha sido encontrada');


    }

    public function disable($id){
        User::where('id', $id)
            ->update(['es_activo' => false]);
        return redirect('/admin/cliente/mostrar/activos');
    }

    public function enable($id){
        User::where('id', $id)
            ->update(['es_activo' => true]);
        return redirect('/admin/cliente/mostrar/pasivos');
    }

    public function modify($id){
        $user = User::where('id', $id)->first();
        return view('cliente.modificar')
            ->with('user',$user);
    }
    public function change($id,Request $request){
        $data = $request->all();
        User::where('id', $id)
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'telefono' => $data['telefono']
            ]);
        return view('cliente.success')
            ->with('message','Cliente modificado exitosamente');
    }
}
